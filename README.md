# my-sample-pipeline

Implemented a streamlined pipeline using GitLab to automate the deployment process of simple html page, integrating various technologies to ensure efficiency and reliability. The pipeline consists of five stages.

## Stages

- Pre-build - Initiates and validates the Terraform project for AWS infrastructure setup. AWS variables are securely provided through GitLab secrets.

- Build (Terraform): Utilizes remote state stored in an S3 bucket to provision EC2 instances and apply VPC networking configurations for public access to the instances.

- Build (Containerize Frontend): Constructs a Docker image containing a simple index.html file and pushes it to a remote DockerHub repository.

- Deploy: Pulls the Docker image and executes it on the EC2 instance, facilitating the deployment of the website.

- Test: Evaluates the availability of the deployed website using Python unittests within a dedicated testing environment.

- Additionally, a destroy stage is implemented to facilitate resource cleanup, leveraging Terraform to dismantle all AWS resources created during the deployment process.
