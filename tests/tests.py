import unittest
import requests
import os


class TestWebsite(unittest.TestCase):

    def setUp(self):
        self.url =  os.getenv('WEBSITE_URL')

    def test_response_code(self):
        response = requests.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_valid_text(self):
        response = requests.get(self.url)
        text_expected = 'Hello world!'
        self.assertIn(text_expected, response.text)

if __name__ == '__main__':
    unittest.main()


