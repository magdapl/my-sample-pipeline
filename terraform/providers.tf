terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.36.0"
    }
  }

  backend "s3" {
    bucket         = "backend-bucket-maddiepka"
    dynamodb_table = "backend-statelock-maddiepka"
    key            = "state/terraform.tfstate"
    region         = "eu-north-1"
    encrypt        = true
  }
}

provider "aws" {
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
  region     = "eu-north-1"
  default_tags {
    tags = {
      project = "my-sample-pipeline"
    }
  }
}