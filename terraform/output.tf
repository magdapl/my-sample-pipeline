output "website_url" {
  description = "Public IP of my ec2 instance"
  value       = aws_instance.my_instance.public_ip
}