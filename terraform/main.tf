
resource "aws_instance" "my_instance" {
  ami                         = var.ec2-ami
  instance_type               = var.ec2-instance-type
  subnet_id                   = aws_subnet.my_public_subnet.id
  vpc_security_group_ids      = [aws_security_group.my_main.id]
  associate_public_ip_address = true
  iam_instance_profile        = aws_iam_instance_profile.my_instance_profile.name
}


resource "aws_iam_role" "my_instance" {
  name               = "my_instance_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "my_instance" {
  role       = aws_iam_role.my_instance.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}


resource "aws_iam_instance_profile" "my_instance_profile" {
  name = "my_instance_profile"

  role = aws_iam_role.my_instance.name
}