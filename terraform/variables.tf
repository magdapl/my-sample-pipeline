variable "AWS_ACCESS_KEY" {
  type = string
}

variable "AWS_SECRET_KEY" {
  type = string
}

variable "pipeline-bucket" {
  type        = string
  description = "Name of pipeline bucket"
  default     = "frontend-bucket-maddiepka"
}

variable "ec2-ami" {
  type        = string
  description = "Ubuntu ami for EC2"
  default     = "ami-0014ce3e52359afbd"
}

variable "ec2-instance-type" {
  type        = string
  description = "Instance type for EC2"
  default     = "t3.micro"
}

